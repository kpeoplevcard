# Xavier Besnard <xavier.besnard@kde.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kpeoplevcard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:35+0000\n"
"PO-Revision-Date: 2021-08-25 08:14+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.08.0\n"

#: src/kpeoplevcard.cpp:69
#, kde-format
msgctxt "given-name family-name"
msgid "%1 %2"
msgstr "%1 %2"
